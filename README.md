# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Summary 
This is the project tebd simulation of the quasi-periodic topological energy pump using the tebd method based on Tenpy (an open source software). 
It is in could be used to other problems with teh tebd/dmrg method base on MPS/MPO
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
I have tried to aviod touch the folder of Tenpy. But sometime we need to make the change of the files directly such as defining the measurement.
This is the case, when we try to do the simulation for time dependent Hamiltonian system using W_I/W_II methods (PRB 91, 165112 (2015)) 
(For this I have changed some of the files to make the (1) 1st order to 2nd order (2) to define more mesurement objects)
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
test_5_step_slatest.py   is the first file written which is self-contained (run separately without any other files)
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact