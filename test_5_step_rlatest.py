sys.path.insert(0,'$HOME/TeNPy/')
import tenpy
import tenpy.linalg.np_conserved as npc
import time
from tenpy.algorithms import tebd
from tenpy.networks.mps import MPS
from tenpy.tools import hdf5_io
from tenpy.tools.hdf5_io import save
from tenpy.tools.hdf5_io import load
#from tenpy.models.model import NearestNeighbotModel

pi = math.pi
#t10 = Timer()
# this is the first step simulation for quasi-periodic energy pump
##################################################general--parameters-------------------------------------------------------------------
omega_1 = 1.
L=20                                      # length need to be an even number
W=.2*omega_1                                      # onsite disorder
U=.08                                     # interaction strength
n_cycles = 6                           # number of cyclings
#ba = np.array(range(L))
#zigp = ba              # from step 4
t_step = [1, 2, 3, 4, 5]
omega_1 = 1.0
omega_2 = 2*omega_1/(1+math.sqrt(5.))          # sweeping/ramping frequency
T = 2*pi/omega_1
J_c = 5*pi/(2*T)

N_ts = 160                                 # same as before each step is divided as 20 mini-step for tebd
N_ts1 = 400                                # step 2 and 4
N_ts2 = 120                                # step 5
Tfifth = T/5
dt = Tfifth/N_ts
dt1 = Tfifth/N_ts1
dt2 = Tfifth/N_ts2

#for ii in ba:
#    zigp[ii] = pow(-1,ba[ii])    
zigp = np.asarray([1,-1]*(L//2))
#---------------------these----for---the--5-step-------Hamiltonian-----------------------------------------------
t11, t12 = J_c*1., 0.                                                      # J_sc as in paper/Mike's code
t_array1 = np.array([(t11 if ii % 2 ==0 else t12) for ii in range(L-1)])     # step 1 and 4  namely Jc-0-Jc-0
t_array2 = np.array([(t12 if ii % 2 ==0 else t11) for ii in range(L-1)])     # step 2 and 3  namely 0-Jc-0-Jc   
t_array3 = np.array([t12 for ii in range(L-1)])                             # step 5
U_array = np.asarray([0.5]+[1]*(L-2)+[0.5])
#---------------------these---are---for---the---current---operator-----------------------------------------------
"""
if we use t_22 = 0, this is make some of the H_bond "None" which will report error when using bond_energies there are two way to work this out
1) make t22 non-zero and do not account it in the final calculation
2) do not use bond_energies directly, only calculate the energy at bond which H_bond is not none and this is reduce the calculation by half so is used in the following code
"""
t21, t22 = J_c*1j, 0.
c_array1 = np.array([(t22 if ii % 2 ==0 else t21) for ii in range(L-1)])    # current operator 1  as in step 2 and 3 above
c_array2 = np.array([(t21 if ii % 2 ==0 else t22) for ii in range(L-1)])    # -----------------2

ranload = load("rand_rec.hdf5")
#ranL = (np.random.random(L)-0.5)*0.2*omega_1
ranL = np.array(ranload['seed'][0:L])
#ranL = np.array([0.,0.,1.,1.])
chem1 = omega_2/2                        # need to given here as Delta in the paper, induced by going to the rotating frame, this is included in the model parameters for 2 and 4
chem2 = 1.*omega_1                       # same as delta in Mike's code (except the multiplication omega_1=1)  on site zeman field for step 5

################################################Current--operator-------------#################################################
Uc=0
#----------------latest-----------now--I--think--it-is-better--to--use--H_bond--instead--of--H_MPO---for--our--current--model------Aug.2021----------------------------
#---------------Here---the--basic--idea---is---to---create--the--current--opeator--as--MPO--using--the--model
# parameters for the first current operator at step 2
model_params_C1 = {
        'L': L,
        'J': c_array1,
        'V': Uc,    # zero
        'mu': Uc,   # zero
        'conserve': 'best',
        'bc_MPS': 'finite'
}

Cur_M1 = tenpy.models.fermions_spinless.FermionChain(model_params_C1)

model_params_C2 = {
        'L': L,
        'J': c_array2,
        'V': Uc,
        'mu':Uc,
        'conserve': 'best',
        'bc_MPS': 'finite'
}


Cur_M2 = tenpy.models.fermions_spinless.FermionChain(model_params_C2)

def bond_energies_RC(mod_inp,psi_inp,start_p=0,Lc=L):
    """ This is the revised code to get the bond energies/work or our model specific --- changed from bond_energies in :class: model
    params: 1) mod_inp   this is the npc arrary :class: model (basically the model Hamiltonian)
            2) psi_inp   which is an object of :class: MPS
            3) start_p   it tells weather we are targeting even or odd bond
            4) Lc        length of the chain
    constant vsites    1D array which indicates the starting point of at which the bond/work is to be calculated
                       For current one it should be range(1,L-1,2)               since it is bond the last index here should be smaller than L-1
                                   two should be    range(0,L-1,2)
    return: E_bond an one day array as decided by 3) baove
    """
    if start_p % 2 ==0:
        vsites = range(1,Lc-1,2)
        return psi_inp.expectation_value(mod_inp.H_bond[2:Lc:2],sites=vsites,axes=(['p0','p1'],['p0*','p1*']))
    else:
        vsites = range(0,Lc-1,2)
        return psi_inp.expectation_value(mod_inp.H_bond[1:Lc:2],sites=vsites,axes=(['p0','p1'],['p0*','p1*']))


#----------------we--will-do--tebd--for--MPO---------------------------------------------------------------------------------------
#------------here--we--need--to--adjust--the--code--to--make--it--does--not--update--S--since--we--donot--have-it-for-MPO-----------normalization--?
#------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------setings-for-the-5-step-model--------------------------------------------------------------------------------------------------------
#---------------------------parameters--for--model-H1-----------------------------------------------------------------------------------------------
model_params1 = {
        'L': L,
        'J': t_array1,
        'V': U,
        'mu':-2.*ranL+0*chem1*zigp+U*U_array,
        'conserve': 'best',
        'bc_MPS': 'finite'
}

#---------------------------parameters--for--model-H2-(or-H2_prime as in Mike's code)-----------------------------------------------------------------------------------------------

model_params2 = {
        'L': L,
        'J': t_array2,
        'V': U,
        'mu':-2.*ranL+chem1*zigp+U*U_array,
        'conserve': 'best',
        'bc_MPS': 'finite'
}
#-------------------------parameters--for--model-H3--------------------------------------------------------------------------------------------------

model_params3 = {
        'L': L,
        'J': t_array2,
        'V': U,
        'mu':-2.*ranL+0*chem1*zigp+U*U_array,
        'conserve': 'best',
        'bc_MPS': 'finite'
}
#--------------------------parameters--for-model-H4---(or-H4_prime as in Mike's code)---------------------------------------------------------------------------------------------
model_params4 = {
        'L': L,
        'J': t_array1,
        'V': U,
        'mu':-2.*ranL-chem1*zigp+U*U_array,
        'conserve': 'best',
        'bc_MPS': 'finite'
}
#-------------------------parameters--for--model-H5-------------------------------------------------------------------------------------------------------------
model_params5 = {
        'L': L,
        'J': t_array3,
        'V': U,
        'mu':-2.*ranL-chem2*zigp+U*U_array,
        'conserve': 'best',
        'bc_MPS': 'finite'
}
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#----------------------settings-for--tebd---engine-----------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
tebd_params1 = {"order": 2,
        "dt": dt,
        "N_steps": N_ts,
#        "start_time": 0.,
        "trunc_params": {"chi_max": 60, "svd_min": 1.e-10}}                                       # set up the parameters for tebd: need to be changed 

#--------------the--reason--we--use--slightly different--pamater--is--a--result--of--default--seting--of-tebd---------------------------------------------------
# it assume time independent Hamiltonian--and--if--params: _U_params is not change it would not recalculate _U but using previous calculation-------------------
tebd_params2 = {"order": 2,
        "dt": dt1,
        "N_steps": 2, #N_ts1
        "trunc_params": {"chi_max": 60, "svd_min": 1.e-10}}                                       # set up the parameters for tebd: need to be changed 

tebd_params3 = {"order": 2,
        "dt": dt2,
        "N_steps": N_ts2,
        "trunc_params": {"chi_max": 60, "svd_min": 1.e-10}}                                       # set up the parameters for tebd: need to be changed 

#------------------input---the--initial---configuration---------------------------------------------------------------------------
M = tenpy.models.fermions_spinless.FermionChain(model_params1)
Ini_psi = ["full"]*(L//2)+["empty"]*(L//2)
psi = MPS.from_product_state(M.lat.mps_sites(), Ini_psi,bc=M.lat.bc_MPS)
#print(psi._B[0]._data)
eng = tebd.TEBDEngine(psi, M, tebd_params1)
#---------------------------------define--measurement-----------------------------------------------------------------------------
def measurementOne(eng,dataOne,CYCLE=0):
    # too maay data, maybe better to save 1 point each cycle? change latter        |(TBD)|   Done
    keys = ['cycle','work_2','t','entropy','trunc_err']
    keys = ['cycle','work_2']
    cycle = CYCLE
    if dataOne is None:
        dataOne = dict([(k,[]) for k in keys])
        return dataOne
    if len(dataOne['cycle']) == cycle+1:
        dataOne['cycle'][cycle] = cycle
        dataOne['work_2'][cycle] = dataOne['work_2'][cycle] + np.sum(bond_energies_RC(Cur_M1,eng.psi,start_p=0))*2*dt1                         #  |(TBD)|   Done
        #dataOne['work_2'][cycle] = dataOne['work_2'][cycle] + np.sum(Cur_M1.bond_energies(eng.psi))*2*dt1                         #  |(TBD)|   Done
        #dataOne['t'][cycle] = eng.evolved_time
        #dataOne['entropy'][cycle] = eng.psi.entanglement_entropy()
        #dataOne['trunc_err'][cycle] = eng.trunc_err.eps
    else:
        dataOne['cycle'].append(cycle)
    # how to put in work 2 should we do some thing additionally or using the code directly?
        dataOne['work_2'].append(np.sum(bond_energies_RC(Cur_M1,eng.psi,start_p=0))*2*dt1)                                              #  |(TBD)|   Done
        #dataOne['work_2'].append(np.sum(Cur_M1.bond_energies(eng.psi))*2*dt1)                                              #  |(TBD)|   Done
        #dataOne['t'].append(eng.evolved_time)
        #dataOne['entropy'].append(eng.psi.entanglement_entropy())
        #dataOne['trunc_err'].append(eng.trunc_err.eps)
    return dataOne

def measurementTwo(eng,dataTwo,CYCLE=0):
    # hack the data as above                                                      |(TBD)|   Done
    cycle = CYCLE
    #keys = ['cycle','work_4','t','entropy','trunc_err']
    keys = ['cycle','work_4']
    #lind = cycle+1
    if dataTwo is None:
        dataTwo = dict([(k,[]) for k in keys])
        return dataTwo
    if len(dataTwo['cycle']) == cycle+1:
        dataTwo['cycle'][cycle] = cycle
        dataTwo['work_4'][cycle] = dataTwo['work_4'][cycle] + np.sum(bond_energies_RC(Cur_M2,eng.psi,start_p=1))*2*dt1                          #   |(TBD)|    Done
        #dataTwo['work_4'][cycle] = dataTwo['work_4'][cycle] + np.sum(Cur_M2.bond_energies(eng.psi))*2*dt1                          #   |(TBD)|    Done
        #dataTwo['t'][cycle] = eng.evolved_time
        #dataTwo['entropy'][cycle] = eng.psi.entanglement_entropy()
        #dataTwo['trunc_err'][cycle] = eng.trunc_err.eps
    else:
        dataTwo['cycle'].append(cycle)
        dataTwo['work_4'].append(np.sum(bond_energies_RC(Cur_M2,eng.psi,start_p=1))*2*dt1)                                                 #  |(TBD)|    Done
        #dataTwo['work_4'].append(np.sum(Cur_M2.bond_energies(eng.psi))*2*dt1)                                                 #  |(TBD)|    Done
        #dataTwo['t'].append(eng.evolved_time)
        #dataTwo['entropy'].append(eng.psi.entanglement_entropy())
        #dataTwo['trunc_err'].append(eng.trunc_err.eps)
    return dataTwo

#--------------------initializing--the--measurement--object-------------------------------------------------
dataOne = measurementOne(eng,None,0)
dataTwo = measurementTwo(eng,None,0)
# initializing the wave function-------above-----------------------------------------------------------
t_start = 0.
t_end = 0.
#----------------------it---seems--it--is--easier-to-define--the--single--site-unitary--by--hand----------------------------
#---------the--formulation--is--c_jc_j^\dagger + exp(i\omega_2 t_i^2(-1)^j)c_j^\dagger c_j----------------------------------
#---------------------------------------------------------------------------------------------------------------------------
chinfo = npc.ChargeInfo([1],['N'])
p_leg = npc.LegCharge.from_qflat(chinfo,[0,1])
B_f2 = []                           # from 0 to L-1 combined even and odd together
B_b2 = []
B_f4 = []
B_b4 = []

B_even = npc.zeros([p_leg,p_leg.conj()],labels=['p','p*'],dtype=np.complex_)
B_odd = npc.zeros([p_leg,p_leg.conj()],labels=['p','p*'],dtype=np.complex_)

for pcycle in range(n_cycles):
    t_2_i = omega_2*(pcycle*T+Tfifth)
    t_2_e = omega_2*(pcycle*T+2*Tfifth)
    t_4_i = omega_2*(pcycle*T+3*Tfifth)
    t_4_e = omega_2*(pcycle*T+4*Tfifth)
    a0 = cmath.exp(0.5j*t_2_i)
    a1 = cmath.exp(-0.5j*t_2_i)
    b0 = cmath.exp(-0.5j*t_2_e)
    b1 = cmath.exp(0.5j*t_2_e)
    #-------U'(t_i^2)-------used-to-get-\psi(t_i^2)'-----------------------------
    B_even[0,0] = 1.
    B_even[1,1] = a0
   # print(B_even)
    B_f2.append(copy.deepcopy(B_even))
    B_odd[0,0] = 1.
    B_odd[1,1] = a1
    B_f2.append(copy.deepcopy(B_odd))
    #------U'(t_e^2)--------used-to-get-\psi(t_e^2)-----------------------------
    B_even[1,1] = b0
    B_odd[1,1] = b1
    B_b2.append(copy.deepcopy(B_even))
    B_b2.append(copy.deepcopy(B_odd))
    #-----------------------------U'(t_i^4)-----used-to-get-\psi(t_i^4)'-----------------------------
    a0 = cmath.exp(-0.5j*t_4_i)
    a1 = cmath.exp(0.5j*t_4_i)
    b0 = cmath.exp(0.5j*t_4_e)
    b1 = cmath.exp(-0.5j*t_4_e)
    B_even[1,1] = a0
    B_odd[1,1] = a1
    B_f4.append(copy.deepcopy(B_even))
    B_f4.append(copy.deepcopy(B_odd))
    #-----------------------------U'(t_e^4)------used-to-get-\psi(t_e^4)-------------------------
    B_even[1,1] = b0
    B_odd[1,1] = b1
    B_b4.append(copy.deepcopy(B_even))
    B_b4.append(copy.deepcopy(B_odd))


# U at single site
# Such an operation has been defined by  apply_local_op(self,i,op,unitary=None,renormalize=False,cutoff=1.3-13)

t10=time.time()
#dataOne.setdefault('psi0',copy.deepcopy(psi))
#print(psi._B[0]._data)
#print(ranL)
for cycle in range(n_cycles):
    print(cycle)
    for i_s in t_step:                 # 5 step calculation


        #--------------------from-here-we-do-tebd-to-get-the-evolution-of-the-wave-function----------------------------------
        if i_s ==1:
            #M = tenpy.models.fermions.FermionChain(model_params1)
            M = tenpy.models.fermions_spinless.FermionChain(model_params1)
            t_start = cycle*T
            #print('psi0_0')
            #for kk in range(4):
            #    print(psi._B[kk]._data)
            #    print(psi._B[kk]._qdata)
            # try to put in the initial time point which is zero by default          |(TBD)|   Down as below/above by updating the dict          
            if cycle>0:
                M = tenpy.models.fermions_spinless.FermionChain(model_params1)
                psi = eng.psi.copy()                         # if this does not work well we will update _B and _S directly         (TBD)
                eng = tebd.TEBDEngine(psi, M, tebd_params1)
                eng.evolved_time = t_start
            eng.run()
            #print('psi1_0')
            #for kk in range(4):
            #    print(eng.psi._B[kk]._data)
            #    print(eng.psi._B[kk]._qdata)
            #dataOne.setdefault('psi1',copy.deepcopy(eng.psi))
            #print()
            # the above gives---------- |\psi(t_e^1)\rangle as in the note Eq.11
        elif i_s ==2:
            M = tenpy.models.fermions_spinless.FermionChain(model_params2)
            t_start = t_start + Tfifth
            t_end = t_start + Tfifth
            psi = eng.psi.copy()
            # Here is the local unitary operation is done on sites by updating _B[i]. _S[i] would not change
            # Checked here tenpy gives the same result as 
            for sindex in np.arange(0,L,2):
                psi.apply_local_op(sindex,B_f2[2*cycle],unitary=True,renormalize=True)                 # even sites 0, 2, 4 , ...
                psi.apply_local_op(sindex+1,B_f2[2*cycle+1],unitary=True,renormalize=True)             # odd sites 1, 3, 5, ...
            #dataOne.setdefault('psi1p',copy.deepcopy(psi))
            #for kk in range(L):
            #    print(psi._B[kk]._data)
            #    print(psi._B[kk]._qdata)
            #psi.apply_local_op(np.arange(0,L,2),B_f2[2*cycle],unitary=True)                # even site
            #psi.apply_local_op(np.arange(1,L,2),B_f2[2*cycle+1],unitary=True)              # odd
            # the above gives--------------- |\psi(t_i^2)'\rangle as given in Eq.12
            eng = tebd.TEBDEngine(psi, M, tebd_params2)
            eng.evolved_time = t_start
            while eng.evolved_time < (t_end-dt1):
                eng.run()     #------------------- |\psi(t_e^2)'\rangle        Eq.13
            # mesurement is done here
                measurementOne(eng,dataOne,CYCLE=cycle)                                    #  |(TBD)|   Done
            #save(dataOne,"./W_2.hdf5",mode="w")
            #dataOne.setdefault('psi2p',copy.deepcopy(eng.psi))
            #print('simulation end time is '+str(eng.evolved_time))
            #print('ideal end time is '+str(t_end))
        elif i_s ==3:
            M = tenpy.models.fermions_spinless.FermionChain(model_params3)
            t_start = t_start + Tfifth
            psi = eng.psi.copy()
            # here the transormation is done to go back the the proper frame
            for sindex in np.arange(0,L,2):
                psi.apply_local_op(sindex,B_b2[2*cycle],unitary=True,renormalize=True)               # even sites
                psi.apply_local_op(sindex+1,B_b2[2*cycle+1],unitary=True,renormalize=True)           # odd sites  # |(TBD)|       Done
            #for kk in range(L):
                #print("This is psi2_e")
                #print(psi._B[kk]._data)
                #print(psi._B[kk]._qdata)
            #dataOne.setdefault('psi2',copy.deepcopy(psi))
            #psi.apply_local_op(np.arange(0,L,2),B_b2[2*cycle],unitary=True)              # even sites
            #psi.apply_local_op(np.arange(1,L,2),B_b2[2*cycle+1],unitary=True)            # odd
            #--------------|\psi(t_e^2)\range----------------------------------- Eq.14
            eng = tebd.TEBDEngine(psi, M, tebd_params1)
            eng.evolved_time = t_start
            eng.run() #----------------------------------|\psi(t_e^3)\rangle--------------Eq.15
        elif i_s ==4:
            M = tenpy.models.fermions_spinless.FermionChain(model_params4)
            t_start = t_start + Tfifth
            t_end = t_start + Tfifth
            psi = eng.psi.copy()
            # Here the transofrmation is done to go the rotating frame/ by updating _B[i] and _S[i]
            for sindex in np.arange(0,L,2):
                psi.apply_local_op(sindex,B_f4[2*cycle],unitary=True,renormalize=True)                # even sites
                psi.apply_local_op(sindex+1,B_f4[2*cycle+1],unitary=True,renormalize=True)            # odd sites # |(TBD)|          Done
            #psi.apply_local_op(np.arange(0,L,2),B_f4[2*cycle],unitary=True)               # even
            #psi.apply_local_op(np.arange(1,L,2),B_f4[2*cycle+1],unitary=True)             # odd
            #--------------|\psi(t_i^4)'\rangle----------------------------Eq.16-----------------
            eng = tebd.TEBDEngine(psi, M, tebd_params2)
            eng.evolved_time = t_start
            while eng.evolved_time < (t_end-dt1):
                eng.run()             #----------------|\psi(t_e^4)'\rangle-----------Eq.17--------------------
                measurementTwo(eng,dataTwo,CYCLE=cycle)                                         # |(TBD)|  Done
        else:
            M = tenpy.models.fermions_spinless.FermionChain(model_params5)
            t_start = t_start + Tfifth
            psi = eng.psi.copy()
            # Here the transformation is done to go back to the proper frame
            for sindex in np.arange(0,L,2):
                psi.apply_local_op(sindex,B_b4[2*cycle],unitary=True,renormalize=True)                # even sites
                psi.apply_local_op(sindex+1,B_b4[2*cycle+1],unitary=True,renormalize=True)            # odd sites   #(TBD)        Done
            #psi.apply_local_op(np.arange(0,L,2),B_b4[2*cycle],unitary=True)
            #psi.apply_local_op(np.arange(1,L,2),B_b4[2*cycle+1],unitary=True)
            #------------------------|\psi(t_e^4)\rangle------------------------Eq.18------------------------------
            eng = tebd.TEBDEngine(psi, M, tebd_params3)
            eng.evolved_time = t_start
            eng.run()
            #---------------------|\psi(t_e^5)\rangle---------------------------Eq.19-------------------------------

t11=time.time()
print("Time used is")
print(t11-t10)
dataOne.setdefault('omega_1',omega_1)
dataOne.setdefault('omega_2',omega_2)
dataOne.setdefault('ranL',ranL)
save(dataOne,"./W_2nn1.hdf5","w")
save(dataTwo,"./W_4nn1.hdf5","w")
