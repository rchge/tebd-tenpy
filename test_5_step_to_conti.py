# this is using the tebd engine directly as we have done for our 5 step model
import numpy as np
import scipy
#import tenpy
import cmath
import math
import copy
import sys
#import h5py
#import json
sys.path.insert(0,'$HOME/TeNPy/')
import tenpy
import tenpy.linalg.np_conserved as npc
import time
from tenpy.algorithms import tebd
from tenpy.networks.mps import MPS
from tenpy.tools import hdf5_io
from tenpy.tools.hdf5_io import save
from tenpy.tools.hdf5_io import load
from tenpy.models.fermions_spinless import FermionChain
#from tenpy.models.model import NearestNeighbotModel

pi = math.pi
#t10 = Timer()
# this is the first step simulation for quasi-periodic energy pump
##################################################general--parameters-------------------------------------------------------------------
omega_1 = 1.
L=30                                      # length need to be an even number
W=.2*omega_1                                      # onsite disorder
U=.08                                     # interaction strength
n_cycles = 1                           # number of cyclings
#ba = np.array(range(L))
#zigp = ba              # from step 4
t_step = [1, 2, 3, 4, 5]
omega_1 = 1.0
omega_2 = 2*omega_1/(1+math.sqrt(5.))          # sweeping/ramping frequency
T = 2*pi/omega_1
delta = 0.2
ampliA = 0.2
Delta0 = 0.3
gamma_0a=0.2
gamma_0b=0.15
randamp = 0.2

N_ts = 1                                 # same as before each step is divided as 20 mini-step for tebd
N_ts1 = 2                                # step 2 and 4
N_ts2 = 2                                # step 5
Tfifth = T/5
dt = Tfifth/N_ts
dt1 = Tfifth/N_ts1
dt2 = Tfifth/N_ts2

Ntot = 160                         # total step per period
Nstep = Ntot//N_ts                # Nstep need to be a factor of Ntot here
t_list = np.array(range(Nstep))*T/Nstep
dt = T/Ntot
#for ii in ba:
#    zigp[ii] = pow(-1,ba[ii])    
zigp = np.asarray([1,-1]*(L//2))
#---------------------these----for---the--5-step-------Hamiltonian-----------------------------------------------
J_c1 = gamma_0a+gamma_0b+ampliA+delta
J_c2 = gamma_0a+gamma_0b-ampliA-delta
t11, t12 = J_c1,J_c2                                                      # J_sc as in paper/Mike's code
t_array1 = np.array([(t11 if ii % 2 ==0 else t12) for ii in range(L-1)])     # step 1 and 4  namely Jc-0-Jc-0
t_array2 = np.array([(t12 if ii % 2 ==0 else t11) for ii in range(L-1)])     # step 2 and 3  namely 0-Jc-0-Jc   
t_array3 = np.array([t12 for ii in range(L-1)])                             # step 5
U_array = np.asarray([0.5]+[1]*(L-2)+[0.5])
#---------------------these---are---for---the---current---operator-----------------------------------------------
"""
if we use t_22 = 0, this is make some of the H_bond "None" which will report error when using bond_energies there are two way to work this out
1) make t22 non-zero and do not account it in the final calculation
2) do not use bond_energies directly, only calculate the energy at bond which H_bond is not none and this is reduce the calculation by half so is used in the following code
"""
t21, t22 = 1.0, 1.0
c_array1 = np.array([(t22 if ii % 2 ==0 else t21) for ii in range(L-1)])    # current operator 1  as in step 2 and 3 above
#c_array2 = np.array([(t21 if ii % 2 ==0 else t22) for ii in range(L-1)])    # -----------------2

ranload = load("rand_ssh.hdf5")
#ranL = (np.random.random(L)-0.5)*0.2*omega_1
ranL = np.array(ranload['seed'][0:L])
#ranL = np.array([0.,0.,1.,1.])
chem1 = 0.                       # same as delta in Mike's code (except the multiplication omega_1=1)  on site zeman field for step 5
suprand = -randamp*ranL + U*U_array
################################################Current--operator-------------#################################################
Uc=0
#----------------latest-----------now--I--think--it-is-better--to--use--H_bond--instead--of--H_MPO---for--our--current--model------Aug.2021----------------------------
#---------------Here---the--basic--idea---is---to---create--the--current--opeator--as--MPO--using--the--model
# parameters for the first current operator at step 2
model_params_C1 = {
        'L': L,
        'J': c_array1,
        'V': Uc,    # zero
        'mu': Uc,   # zero
        'conserve': 'best',
        'bc_MPS': 'finite'
}

CurrO21 = FermionChain(model_params_C1)

chinfo = npc.ChargeInfo([1],['N'])
p_leg = npc.LegCharge.from_qflat(chinfo,[0,1])
CurrO22 = npc.zeros([p_leg,p_leg.conj()],labels=['p','p*'])
CurrO22[1,1] = 1.

model_params_C2 = {
        'L': L,
        'J': -c_array1*zigp[0:L-1],
        'V': Uc,    # zero
        'mu': Uc,   # zero
        'conserve': 'best',
        'bc_MPS': 'finite'
}

CurrO11 = FermionChain(model_params_C2)
#----------from------the-----time----dependent-----------Hamiltonian-------------------------------------
model_params1 = {
        'L': L,
        'J': t_array1,                               # need to be changed
        'V': U,
        'mu':suprand-chem1*zigp,         # need to be changed
        'conserve': 'best',
        'bc_MPS': 'finite'
}
#----------------------settings-for--tebd---engine-----------------------------------------------------------------------------------------------------------------------------------
tebd_params1 = {"order": 2,
        "dt": dt,
        "N_steps": N_ts,
#        "start_time": 0.,
        "trunc_params": {"chi_max": 120, "svd_min": 1.e-10}}                                       # set up the parameters for tebd: need to be changed 
#------------------input---the--initial---configuration---------------------------------------------------------------------------
dint = N_ts*dt
M = FermionChain(model_params1)
Ini_psi = ["full"]*(L//2)+["empty"]*(L//2)
psi = MPS.from_product_state(M.lat.mps_sites(), Ini_psi,bc=M.lat.bc_MPS)
#print(psi._B[0]._data)
eng = tebd.TEBDEngine(psi, M, tebd_params1)
#---------------------------------define--measurement-----------------------------------------------------------------------------
def measurementOne(eng,dataOne,CYCLE=0,ts=0):
    # too maay data, maybe better to save 1 point each cycle? change latter        |(TBD)|   Done
    #keys = ['cycle','work_2','t','entropy','trunc_err']
    keys = ['cycle','work_21','work_22','work_11','work_12']
    cycle = CYCLE
    if dataOne is None:
        dataOne = dict([(k,[]) for k in keys])
        return dataOne
    #print('psi')
    #for ii in range(L):
    #    print('site number is'+str(ii))
    #    print(eng.psi._B[ii]._data)
    #    print(eng.psi._B[ii]._qdata)
    workO22 = np.sum(np.array([-1,1]*(L//2))*np.array(eng.psi.expectation_value(CurrO22)))*dint
    if len(dataOne['cycle']) == cycle+1:
        dataOne['cycle'][cycle] = cycle
        dataOne['work_21'][cycle] = dataOne['work_21'][cycle] + np.sum(CurrO21.bond_energies(eng.psi))*gamma_0b*math.sin(omega_2*ts)*dint                         #  |(TBD)|   Done
        dataOne['work_11'][cycle] = dataOne['work_11'][cycle] + np.sum(CurrO11.bond_energies(eng.psi))*ampliA*math.sin(omega_1*ts)*dint
        dataOne['work_22'][cycle] = dataOne['work_22'][cycle] + workO22*Delta0*math.cos(omega_2*ts)*(1+math.cos(omega_1*ts))
        dataOne['work_12'][cycle] = dataOne['work_12'][cycle] - workO22*Delta0*math.sin(omega_2*ts)*math.sin(omega_1*ts)
        #dataOne['trunc_err'][cycle] = eng.trunc_err.eps
    else:
        dataOne['cycle'].append(cycle)
    # how to put in work 2 should we do some thing additionally or using the code directly?
        dataOne['work_21'].append(np.sum(CurrO21.bond_energies(eng.psi))*gamma_0b*math.sin(omega_2*ts)*dint)                                              #  |(TBD)|   Done
        dataOne['work_11'].append(np.sum(CurrO11.bond_energies(eng.psi))*ampliA*math.sin(omega_1*ts)*dint)
        dataOne['work_22'].append(workO22*Delta0*math.cos(omega_2*ts)*(1+math.cos(omega_1*ts)))
        dataOne['work_12'].append(-workO22*Delta0*math.sin(omega_2*ts)*math.sin(omega_1*ts))
        #dataOne['trunc_err'].append(eng.trunc_err.eps)
    return dataOne

#--------------------initializing--the--measurement--object-------------------------------------------------
dataOne = measurementOne(eng,None,0)
# initializing the wave function-------above-----------------------------------------------------------
t_start = 0.
t_end = 0.
#---------------------------------------------------------------------------------------------------------------------------
st10=time.time()
for cycle in range(n_cycles):
    print(cycle)
    for i_s in range(Nstep):                 # 5 step calculation

        #--------------------from-here-we-do-tebd-to-get-the-evolution-of-the-wave-function----------------------------------
            #M = tenpy.models.fermions.FermionChain(model_params1)
            M = FermionChain(model_params1)
            t_start = cycle*T + t_list[i_s]
            #print('psi0_0')
            #for kk in range(4):
            #    print(psi._B[kk]._data)
            #    print(psi._B[kk]._qdata)
            # try to put in the initial time point which is zero by default          |(TBD)|   Down as below/above by updating the dict          
            if t_start>0:
                t11 = gamma_0a+delta+gamma_0b*math.cos(omega_2*t_start)+ampliA*math.cos(omega_1*t_start)
                t12 = gamma_0a-delta+gamma_0b*math.cos(omega_2*t_start)-ampliA*math.cos(omega_1*t_start)
                t_array1 = np.array([(t11 if ii % 2 ==0 else t12) for ii in range(L-1)])
                model_params1['J'] = t_array1
                chem1 = Delta0*math.sin(omega_2*t_start)*(1.+math.cos(omega_1*t_start))
                model_params1['mu'] = suprand-chem1*zigp
                M = FermionChain(model_params1)
                psi = eng.psi.copy()                         # if this does not work well we will update _B and _S directly         (TBD)
                eng = tebd.TEBDEngine(psi, M, tebd_params1)
                eng.evolved_time = t_start
            eng.run()
            measurementOne(eng,dataOne,CYCLE=cycle*Nstep+i_s,ts=t_start)
            #if(t_start<t_list[4]):
            #    print('eng.evolved_time = '+str(t_start))
            #    #print('H_bond is')
            #    #for jj in range(1,L):
            #        #print('site = '+str(jj))
            #        #print(M.H_bond[jj]._data)
            #        print(M.H_bond[jj]._qdata)
            #    print
            #    print('psi1_0')
            #    for kk in range(L):
            #        print('site = '+str(kk))
            #        print(eng.psi._B[kk]._data)
            #        print(eng.psi._B[kk]._qdata)
               # dataOne.setdefault('psi1',copy.deepcopy(eng.psi))
               # print()

st11=time.time()
print("Time used is")
print(st11-st10)
dataOne.setdefault('omega_1',omega_1)
dataOne.setdefault('omega_2',omega_2)
dataOne.setdefault('ranL',ranL)
save(dataOne,"./Contissh_test.hdf5","w")
#print('This is the work for mode 2')
#print(dataOne['work_21'])
#print(np.sum(dataOne['work_21']))
#print(dataOne['work_22'])
#print(np.sum(dataOne['work_22']))
